# Fuber

Fuber REST Api can be used by customers to book a cab for travel. Customers should have an account in database for booking.
Fare is calculate as per the given conditiona and currency used to display fare is Dogecoin.
While booking a cab customer can specify color of cab like white, pink etc. Pink cabs have extra fare cost. Customer is also required to specify latitude and longitude of destination location.

## Assumptions

I have used Location class which accepts latitude and longitude as its parameter.
Customer is always created in database with a location that is latitude and longitude. Cab is also registered in system with a location.
Distance is always specified in kms. All cabs are assumed to run at constant speed of 1km/hr. So time take to cover any distance is equal to distance.


## Getting Started

Follow below instructions to understand the dependencies and get system running on development and test environments.

### Prerequisites

Following lines specfies on which ruby version this application is tested on. Also use rvm to maintain ruby versions.

```
rvm install 2.7
```

For Database mongodb is used. Install mongodb on your machine. Below instructions are specified for mac system.

```
brew tap mongodb/brew
brew install mongodb-community@4.2
```

### Installing

Used bundler to bundle the gems mentioned in gemfile before starting the server.

```
bundle install
```
To run MongoDB as a macOS service. You will need the service running to use in application.

```
brew services start mongodb-community@4.2
```

### Development

To start the server locally use following command:

```
rackup -p <port number>
```

for example:

```
rackup -p 4000
```

If no port number is specified it starts server at port 9292

## Running the tests

For running the automated test cases use the following instruction

```
rspec
```

## Populate database

For ease of checking the working of system, populate.rb file is generated which can be used to make dummy entries in database of cabs and customers.

## Built With

* [Sinatra](http://sinatrarb.com/) - The web framework used
* [MongoDB](https://www.mongodb.com/) - Database
* [Rspec](https://rspec.info/) - Used to generate automated testt cases