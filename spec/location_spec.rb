require 'spec_helper.rb'

RSpec.describe Location do
  let(:location){Location.new}

  describe '#initialize' do
    context 'with arguments' do
      it 'returns invalid object as no name' do
        expect(location.valid?).to eq(false)
      end

      it 'returns valid object as name is present' do
        location.latitude = 50
        location.longitude = 20

        expect(location.valid?).to eq(true)
      end
    end
  end

  describe '#distance_from' do
    it 'returns eucledian distance from source location' do
      location.latitude, location.longitude = 15, 25
      expect(location.distance_from(Location.new(latitude: 22, longitude: 50)).to_i).to eq(25)
    end
  end

  describe '#calculate_fare' do
    it 'returns total amount of white ride' do
      location.latitude, location.longitude = 15, 25
      cab = Cab.create!(color: 'white', available: true, avg_speed: 1, location: Location.new(latitude: 5, longitude:  10))
      expect(location.calculate_fare(Location.new(latitude: 22, longitude: 50), cab).to_i).to eq(77)
    end

    it 'returns total amount of pink ride' do
      location.latitude, location.longitude = 15, 25
      cab = Cab.create!(color: 'pink', available: true, avg_speed: 1, location: Location.new(latitude: 5, longitude:  10))
      expect(location.calculate_fare(Location.new(latitude: 22, longitude: 50), cab).to_i).to eq(82)
    end
  end
end