require 'spec_helper.rb'

RSpec.describe Cab do

  before(:each) do
    Mongoid.purge!
  end

  let!(:customer){
    location = Location.new(latitude: 20, longitude: 30)
    Customer.create!(name: 'test1', location: location)
  }

  let!(:cab1) {
    location = Location.new(latitude: 10, longitude: 20)
    Cab.create!(color: 'white', location: location, available: true)
  }

  let!(:cab2) {
    location = Location.new(latitude: 14, longitude: 15)
    Cab.create!(color: 'black', location: location, available: true)
  }

  let!(:cab3) {
    location = Location.new(latitude: 22, longitude: 25)
    Cab.create!(color: 'pink', location: location, available: true)
  }

  describe '.get_nearest_available_cab' do

    context 'when no car is available' do
      before(:each) do
        [cab1, cab2, cab3].each{|cab| engage_cab(cab)}
      end

      it 'returns nil' do
        available_cab = Cab.get_nearest_available_cab(customer)
        expect(available_cab).to be_nil
      end
    end

    context 'when multiple cars are available' do
      it 'returns the nearest one' do
        available_cab = Cab.get_nearest_available_cab(customer.location)
        expect(available_cab).to eq(cab3)
      end

      context 'when particular color is passed' do
        it 'return car with particular color' do
          available_cab = Cab.get_nearest_available_cab(customer.location, 'white')
          expect(available_cab).to eq(cab1)
        end

        context 'when color car is not available' do
          it 'returns nil' do
            available_cab = Cab.get_nearest_available_cab(customer.location, 'blue')
            expect(available_cab).to be_nil
          end
        end
      end
    end
  end

  describe '#start_ride' do
    context 'when customer has already ongoing ride' do
      let(:end_location){
        Location.new(longitude: 30, latitude: 30)
      }
      let(:customer2){
        location = Location.new(latitude: 30, longitude: 40)
        Customer.create!(name: 'test2', location: location)
      }
      it 'returns false' do
        cab1.start_ride(customer, end_location)
        ride_started = cab2.start_ride(customer, end_location)
        expect(ride_started).to be false
      end

      context 'when the cab has already ongoing ride' do
        it 'returns false' do
          cab1.start_ride(customer, end_location)
          ride_started = cab1.start_ride(customer2, end_location)
          expect(ride_started).to be false
        end
      end

      context 'when customer and cab both are available' do
        it 'marks cab not available' do
          ride_started = cab1.start_ride(customer2, end_location)
          expect(cab1.available).to be false
        end

        it 'returns true' do
          ride_started = cab1.start_ride(customer2, end_location)
          expect(ride_started).to be true
        end
      end
    end
  end

  def engage_cab(cab)
    location = Location.new(latitude: 15, longitude: 10)
    customer = Customer.create!(name: 'test2', location: location)
    end_location = Location.new(latitude: 30, longitude: 20)
    cab.start_ride(customer, end_location)
  end

end