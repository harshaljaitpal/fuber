require 'spec_helper.rb'

RSpec.describe Trip do
  let(:trip){Trip.new}

  describe '#initialize' do
    context 'with arguments' do
      it 'returns invalid object as no name' do
        expect(trip.valid?).to eq(false)
      end

      it 'returns valid object as name is present' do
        trip.customer = Customer.new(name: 'ABC')
        trip.cab      = Cab.new(color: 'white', available: true, avg_speed: 1)
        expect(trip.valid?).to eq(true)
      end
    end
  end

  describe '#start ride' do
    it 'it changes state of ride' do
      cab = Cab.create!(color: 'white', available: true, avg_speed: 1, location: Location.new(latitude: 5, longitude:  10))
      customer = Customer.create!(name: 'ABC', location: Location.new(latitude: 18, longitude: 20))
      trip.customer = customer
      trip.cab = cab
      trip.start_ride
      expect(trip.state).to eq('riding')
      expect(trip.cab.available).to eq(false)
    end

    it 'it do not changes state of ride due to invalid arguments' do
      trip.start_ride
      expect(trip.state).to eq('new')
    end
  end

  describe '#end ride' do
    it 'it changes state of ride for white ride' do
      cab = Cab.create!(color: 'white', available: true, avg_speed: 1, location: Location.new(latitude: 5, longitude:  10))
      customer = Customer.create!(name: 'ABC', location: Location.new(latitude: 18, longitude: 20))
      trip.customer = customer
      trip.cab = cab
      trip.source = customer.location
      trip.destination = Location.new(latitude: 45, longitude: 50)
      trip.start_ride
      trip.end_ride
      expect(trip.state).to eq('completed')
      expect(trip.total_fare.to_i).to eq(121)
      expect(trip.cab.available).to eq(true)
    end

    it 'it changes state of ride for pink ride' do
      cab = Cab.create!(color: 'pink', available: true, avg_speed: 1, location: Location.new(latitude: 5, longitude:  10))
      customer = Customer.create!(name: 'ABC', location: Location.new(latitude: 18, longitude: 20))
      trip.customer = customer
      trip.cab = cab
      trip.source = customer.location
      trip.destination = Location.new(latitude: 45, longitude: 50)
      trip.start_ride
      trip.end_ride
      expect(trip.state).to eq('completed')
      expect(trip.total_fare.to_i).to eq(126)
      expect(trip.cab.available).to eq(true)
    end

    it 'it do not changes state of ride due to invalid arguments' do
      trip.end_ride
      expect(trip.state).to eq('new')
    end
  end
end