require 'spec_helper.rb'

RSpec.describe Customer do
  let(:customer){Customer.new}

  describe '#initialize' do
    context 'with arguments' do
      it 'returns invalid object as no name' do
        expect(customer.valid?).to eq(false)
      end

      it 'returns valid object as name is present' do
        customer.name = 'ABC'
        expect(customer.valid?).to eq(true)
      end
    end
  end

  describe '#has_ongoing_ride' do
    it 'returns true when ride already present' do
      cab = Cab.create!(color: 'white', available: true, avg_speed: 1, location: Location.new(latitude: 5, longitude:  10))
      trip = customer.trips.build(cab: cab, source: Location.new(latitude: 18, longitude: 20), destination: Location.new(latitude: 50, longitude: 100))
      trip.start_ride
      expect(customer.has_ongoing_ride?).to eq(true)
    end

    it 'returns false when no ride present' do
      expect(customer.has_ongoing_ride?).to eq(false)
    end
  end
end