require 'rubygems'
require 'bundler'
Bundler.require(:default)

Mongoid.load!(File.dirname(__FILE__) + '/mongoid.config')

Dir[File.dirname(__FILE__) + '/../app/models/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/../app/serializers/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/../app/controllers/*.rb'].each {|file| require file }

