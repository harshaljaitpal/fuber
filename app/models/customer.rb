class Customer
  include Mongoid::Document

  field :name, type: String

  validates :name, presence: true

  has_many :trips
  embeds_one :location

  def has_ongoing_ride?
    trips.with_state(:riding).exists?
  end
end