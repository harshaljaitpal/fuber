class Trip
  include Mongoid::Document

  field :total_fare, type: Float
  field :state, type: String

  validates :customer, presence: true
  validates :cab, presence: true

  belongs_to :cab
  belongs_to :customer
  embeds_one :source, class_name: 'Location', inverse_of: :source_location
  embeds_one :destination, class_name: 'Location', inverse_of: :destination_location

  state_machine initial: :new do
    before_transition any => :riding do |trip, transition|
      trip.total_fare = 0
    end

    after_transition to: :riding do |trip|
      trip.cab.update_attributes(available: false, avg_speed: 1)
    end

    before_transition to: :completed do |trip|
      trip.total_fare = trip.source.calculate_fare(trip.destination, trip.cab)
    end

    after_transition to: :completed do |trip|
      trip.cab.update_attributes(location: trip.destination, available: true)
    end

    event :start_ride do
      transition new: :riding, if: :check_if_valid_trip?
    end

    event :end_ride do
      transition riding: :completed, if: :check_if_valid_trip?
    end
  end

  private

  def  check_if_valid_trip?
    customer.present? && cab.present?
  end
end