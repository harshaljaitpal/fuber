class Cab
  include Mongoid::Document

  field :color, type: String
  field :available, type: Boolean
  field :avg_speed, type: Integer

  validates :color, presence: true
  validates :available, inclusion: { in: [ true, false ] }

  scope :with_color, -> (color) { where(color: color.downcase) }
  scope :available, -> { where(available: true) }

  has_many :trips
  embeds_one :location

  def self.get_nearest_available_cab(location, color=nil)
    cabs = Cab.available
    cabs = cabs.with_color(color) if color.present?
    cabs.each.min_by do |cab|
      cab.location.distance_from(location)
    end
  end

  def start_ride(customer, end_location)
    return false if customer.has_ongoing_ride?
    return false unless available
    trip = trips.build(customer: customer, source: customer.location, destination: end_location)
    trip.start_ride
  end

  def car_cost
    color == 'pink' ? 5 : 0
  end
end