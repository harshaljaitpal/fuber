class Location
  include Mongoid::Document

  field :latitude, type: Float
  field :longitude, type: Float

  validates :latitude, presence: true
  validates :longitude, presence: true

  embedded_in :cab
  embedded_in :customer
  embedded_in :source_location, class_name: 'Trip', inverse_of: :source
  embedded_in :destination_location, class_name: 'Trip', inverse_of: :destination

  def distance_from(source)
    (Math.sqrt((source.latitude - self.latitude)**2 + (source.longitude. - self.longitude)**2)).round(2)
  end

  def calculate_fare(source, cab)
    total_distance = self.distance_from(source)
    ((total_distance * cab.avg_speed) + (total_distance * 2) + cab.car_cost).round(2)
  end
end
