class Fuber < Sinatra::Base
  register Sinatra::Namespace
  get '/' do
    'Welcome to Fuber!'
  end

  namespace '/api/v1' do
    before do
      content_type 'application/json'
    end

    helpers do
      def find_customer(id)
        Customer.find(id)
      rescue
        halt 400, { message: 'Your account was not found !' }.to_json
      end
    end

    get '/cabs' do
      Cab.all.map { |cab| CabSerializer.new(cab) }.to_json
    end

    get '/customers' do
      Customer.all.map {|c| c.to_json}
    end

    get '/customers/:id/start_ride' do
      customer = find_customer(params[:id])
      color = params[:color]

      cab = Cab.get_nearest_available_cab(customer.location, color)

      end_location = Location.new(latitude: params[:latitude], longitude: params[:longitude])
      trip_started = cab.start_ride(customer, end_location) if cab
      if trip_started
        status 200
        body CabSerializer.new(cab).to_json
      else
        status 422
        body "There are no cabs available for you right now!!"
      end
    end

    get '/customers/:id/end_ride' do
      customer = find_customer(params[:id])
      trip = customer.trips.with_state(:riding).first
      if trip && trip.end_ride
        status 200
        body TripSerializer.new(trip).to_json
      else
        halt 404, {message: 'Trip was not found or trip already ended !'}.to_json
      end
    end
  end
end