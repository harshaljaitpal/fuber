class TripSerializer
  def initialize(trip)
    @trip = trip
  end

  def as_json(*)
    data = {
      amount: "#{@trip.total_fare} Dogecoin"
    }
    data[:errors] = @trip.errors if @trip.errors.any?
    data
  end
end