class CabSerializer
  def initialize(cab)
    @cab = cab
  end

  def as_json(*)
    data = {
      color: @cab.color,
      latitude: @cab.location.latitude,
      longitude: @cab.location.longitude
    }
    data[:errors] = @cab.errors if @cab.errors.any?
    data
  end
end