#This file is used to load dummy data to into my database

require_relative 'config/init'
['white', 'white', 'pink', 'black', 'pink'].each do |color|
  location = Location.new(latitude: rand(51), longitude: rand(51))
  Cab.create!(color: color, available: true, avg_speed: 30, location: location)
end

10.times do
  location = Location.new(latitude: rand(51), longitude: rand(51))
  Customer.create!(location: location, name: 'ramesh')
end
